# Projeto Integrador (2020/1) Sistema de aprendizado.
## *Reunião 23/03/2020 (Sprint NULL)*:
  Foi discutido entre os membros, as características do sistema, tais como sistema de pontuação  e aprendizado. 
Logo após foi especificado as tarefas que deveriam ser criadas para se chegar ao desejado, sem especificar 
os pormenores de cada item, sendo opinado por ambos integrantes.
Logo apos a finalizar as estórias, ambos fizeram melhor detalhamento das tarefas que seriam realizadas primeiro. 

Resumo da aplicação:

O programa será baseado na plataforma de idiomas duolingo, com a intenção de melhorar a gameficação já proposta pela plataforma. As novas ideias a serem implementadas serão, um ranking global baseado no nivel do usuário, opções de avatares conforme o progresso em suas atividades e temática de jogo em atividades, como por exemplo chefe final ao fim de cada unidade.  
As tarefas ocorrerão da seguinte forma: temos 3 níveis de dificuldade, fácil, médio e difícil, cada atividade terá 10 perguntas divididas entre 5 assertivas e 5 dissertativas, a pontuação de cada uma será a quantidade de questões certas multiplicado pela dificuldade. Ao final de cada unidade de aprendizado, o usuário deverá enfrentar um chefe, respondendo perguntas aleatórias da unidade, cada questão vale 1 de dano. O chefe terá 7 de vida enquanto o usuário 3, assim ele será forçado a acertar 70% para passar para a próxima unidade.  
O sistema de ranquamento será global, quanto mais experiência o usuário tiver, mais alta será sua posição.

## *Reunião 08/04/2020 (Sprint NULL)*:

  Montagem de estórias e IML do banco de dados, que optamos por usar MySQL 8.0 como gerenciador.
